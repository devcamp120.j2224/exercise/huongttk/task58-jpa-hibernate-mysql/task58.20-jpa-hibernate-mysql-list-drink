package com.devcamp.task58s20.drinkapi.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.task58s20.drinkapi.model.CDrink;
import com.devcamp.task58s20.drinkapi.repository.IDrinkRepository;

@RestController
@CrossOrigin
@RequestMapping("/")
public class CDrinkController {
    @Autowired
    IDrinkRepository drinkRepository;

    @GetMapping("/drinks")
    public ResponseEntity <List<CDrink>> getAllDrink(){
        try {
            List<CDrink> listDrink = new ArrayList<CDrink>();
            drinkRepository.findAll().forEach(listDrink :: add);
            return new ResponseEntity<>(listDrink, HttpStatus.OK);
        }catch (Exception ex){
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
